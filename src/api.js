export function fetchGify(query, offset) {
  return fetch(
    `https://api.giphy.com/v1/gifs/search?api_key=sXpGFDGZs0Dv1mmNFvYaGUvYwKX0PWIh&limit=30&offset=${offset}&q=${query}`
  );
}

// https://api.giphy.com/v1/gifs/trending?api_key=sXpGFDGZs0Dv1mmNFvYaGUvYwKX0PWIh
