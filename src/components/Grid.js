import InfiniteScroll from "react-infinite-scroll-component";

export function Grid({ items = [], fetchData }) {
  console.log(items);
  return (
    <InfiniteScroll
      dataLength={items.length} //This is important field to render the next data
      next={fetchData}
      hasMore={true}
      loader={<h4>Loading...</h4>}
      endMessage={
        <p style={{ textAlign: "center" }}>
          <b>Yay! You have seen it all</b>
        </p>
      }
    >
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {items.map((_, index) => (
          <div style={{ border: "1px solid black" }} key={index}>
            <img src={_.images.downsized.url} alt="gify" />
          </div>
        ))}
      </div>
    </InfiniteScroll>
  );
}
