import React, { useState } from "react";
import SearchInput from "./components/SearchInput";
import { fetchGify } from "./api";
import { Grid } from "./components/Grid";

function App() {
  const [results, setResults] = useState(null);
  const [query2, setQuery2] = useState("");

  function handleSearch(query, offset = 0) {
    setQuery2(query);
    fetchGify(query, offset)
      .then((response) => response.json())
      .then((parsedResponse) => {
        if (results) {
          setResults((prev) => ({
            ...parsedResponse,
            data: [...prev?.data, ...parsedResponse.data],
          }));
        } else {
          setResults(parsedResponse);
        }
      });
  }

  return (
    <div className="App">
      <header className="App-header">
        <SearchInput onSearch={handleSearch} />
      </header>
      <main>
        <Grid
          items={results?.data}
          fetchData={() => {
            handleSearch(query2, results?.data.length / 30);
          }}
        />
      </main>
    </div>
  );
}

export default App;
